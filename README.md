# OpenML dataset: Cinema-Tickets

https://www.openml.org/d/43388

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Cinema industry is not excluded of getting advantage of predictive modeling. Like other industry it
can help cinemas for cost reduction and better ROI. By forecasting sale,  screening in different location could be optimized as well as effective market targeting and pricing. 
Also historical data of sale and movies details e.g. cost, cast and crews, and other project details like schedule,  could help producers to select high performance cast and crews and planning for better projects  ROI . Also it helps to assign  screening location  on hot spots and areas.  
 
Content
About eight months sales  history of different cinemas with detailed data of screening , during 2018  with encoded annonymized locations .  
Starter Kernels EDA , Temporal Feat Eng and XGBoost 

Inspiration
Time series analysis
Cinema Clustering
Forecast sales for each cinema
Recommendation:
Movie genre recommendation for cinemas
Cinema location recommendation
Cast and crew ratings

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43388) of an [OpenML dataset](https://www.openml.org/d/43388). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43388/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43388/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43388/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

